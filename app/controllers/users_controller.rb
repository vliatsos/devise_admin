class UsersController < ApplicationController
 before_filter :authenticate_user!
  before_filter :get_user, :only => [:index, :new, :edit]
  before_filter :accessible_roles, :only => [:new, :edit, :show, :update, :create]
  load_and_authorize_resource :only => [:show, :new, :destroy, :edit, :create]

  # GET /users
  # GET /users.xml                                                
  # GET /users.json                                       HTML and AJAX
  #-----------------------------------------------------------------------
  def index
    @users = User.accessible_by(current_ability, :index)
    respond_to do |format|
      format.html
      format.json { render :json => @users }
      format.xml  { render :xml => @users }
    end
  end
 
  # GET /users/new
  # GET /users/new.xml                                            
  # GET /users/new.json                                    HTML AND AJAX
  #-------------------------------------------------------------------
  def new
    respond_to do |format|
      format.html
      format.json { render :json => @user }   
      format.xml  { render :xml => @user }
    end
  end
 
  # GET /users/1
  # GET /users/1.xml                                                       
  # GET /users/1.json                                     HTML AND AJAX
  #-------------------------------------------------------------------
  def show
    respond_to do |format|
      format.html      
      format.json { render :json => @user }
      format.xml  { render :xml => @user }
    end
 
  rescue ActiveRecord::RecordNotFound
    respond_to_not_found(:json, :xml, :html)
  end
 
  # GET /users/1/edit                                                      
  # GET /users/1/edit.xml                                                      
  # GET /users/1/edit.json                                HTML AND AJAX
  #-------------------------------------------------------------------
  def edit
    respond_to do |format|
      format.html
      format.json { render :json => @user }   
      format.xml  { render :xml => @user }
    end
 
  rescue ActiveRecord::RecordNotFound
    respond_to_not_found(:json, :xml, :html)
  end
 
  # DELETE /users/1     
  # DELETE /users/1.xml
  # DELETE /users/1.json                                  HTML AND AJAX
  #-------------------------------------------------------------------
  def destroy
    @user.destroy
    flash[:notice] = "User deleted."
    redirect_to(:action => :index)
  rescue ActiveRecord::RecordNotFound
    respond_to_not_found(:html, :json, :xml)
  end
 
  # POST /users
  # POST /users.xml         
  # POST /users.json                                      HTML AND AJAX
  #-----------------------------------------------------------------
  def create
    @user = User.new(user_params)
    # authorize! :create, @user
    if @user.save
      respond_to do |format|
        format.html { redirect_to :action => :index }
        format.json { render :json => @user.to_json, :status => 200 }
        format.xml  { head :ok }
      end
    else
      respond_to do |format|
        format.html { render :action => :new, :status => :unprocessable_entity }
        format.json { render :plain => "Could not create user", :status => :unprocessable_entity } # placeholder
        format.xml  { head :ok }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.xml
  # PUT /users/1.json                                            HTML AND AJAX
  #----------------------------------------------------------------------------
  def update
    @user = User.find(params[:id])
    # authorize! :update, @user
    if params[:user][:password].blank?
      [:password,:password_confirmation,:current_password].collect{|p| params[:user].delete(p) }
    else
      @user.errors.add(:current_password, "The password you entered is incorrect") unless @user.valid_password?(params[:user][:current_password])
    end
    respond_to do |format|
      if @user.errors.empty? and @user.update_attributes(user_params)
        #logger.debug params[:user]
        flash[:notice] = "Your account has been updated"
        format.html { render :action => :edit }
        format.json { render :json => @user.to_json, :status => 200 }
        format.xml  { head :ok }
      else
        format.html { render :action => :edit, :status => :unprocessable_entity }
        format.json { render :plain => "Could not update user", :status => :unprocessable_entity } #placeholder
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
 
  rescue ActiveRecord::RecordNotFound
    respond_to_not_found(:js, :xml, :html)
  end

protected

  # Get roles accessible by the current user
  #----------------------------------------------------
  def accessible_roles
    @accessible_roles = Role.accessible_by(current_ability,:read)
  end
 
  # Make the current user object available to views
  #----------------------------------------
  def get_user
    @current_user = current_user
  end

  def user_params
    params.require(:user).permit(
                                 :email, 
                                 :password,
                                 :password_confirmation,
                                 :current_password,
                                 role_ids: []
                                 )
  end
end
